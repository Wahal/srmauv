#!/usr/bin/env python

#-----------------------------------------#
# Name: Mrinal Wahal                      #
# Degree: B-Tech (CSE), 1st Year          #
# Script: SRMAUV Recruitment Coding Tasks #
#-----------------------------------------#

#Note: In order to run the programs, kindly remove the hash(#) from in front of them.
#This will make the function callable.
#Otherwise python will treat it as a comment.

import time, random

print ("-")*50
print ('The SRMAUV Recruitment Tasks by Mrinal Wahal')
print ("-")*50

print ("\nNote: This particular script belongs to Mrinal Wahal and him only.\
This script has only been wriiten/coded for the particular purpose of SRMAUV recruitment process.\
No one is warranetd to misuse/hamper with the script without Mrinal's permission.")

print ("\nNOTE: Press 'Ctrl+C' if you wish to terminate the program in between.\n")

#------------------------------------------------------------------------------

#This is the first task to calculate the average degree from given user inputs.
def average():

    lx = raw_input("Enter degree values seperated by comma: ").split(',')
    l = [int(x) for x in lx]
    return (sum(l)/len(l))

#Remove the hash(#) from next line in order to make the 1st program work.
#print ("The Average Degree is: %d" % (average()))

#------------------------------------------------------------------------------

#This is the second task to verify a nxn Sudoko.
def verify_sudoku(game):
    n = len(game)
    if n < 1:
        return False
    for i in range(0, n):
        horizontal = []
        vertical = []
        for k in range(0, n):
            #vertical check
            if game[k][i] in vertical:
                return False
            vertical.append(game[k][i])

            if game[i][k] in horizontal:
                return False
            horizontal.append(game[i][k])
    return True

#I use the following sudoko to check [[1,3,2],[2,1,3],[3,2,1]].
#Remove the hash(#) from next line in order to make the 2nd program work.
#print "The Sudoku Verification is complete. And it's", verify_sudoku([[1,3,2],[2,1,3],[3,2,1]])

#-----------------------------------------------------------------------------

#This is the 3rd Task to Make a Postfix Calculator.
def postfix_calculator():

    stack = []
    user_input = raw_input("Enter Equation: ")
    tokens = []
    for x in user_input:
        if x.isdigit(): tokens.append(int(x))
        else: tokens.append(x)

    for a in tokens:
        if type(a) is int:
            stack.append(a)
            continue

        op1, op2 = stack.pop(), stack.pop()

        if a == '+':
            stack.append(op2 + op1)
        elif a == '-':
            stack.append(op2 - op1)
        elif a == '*':
            stack.append(op2 * op1)
        elif a == '/':
            stack.append(op2 / op1)
    return stack.pop()

#Remove the hash(#) from next line in order to make the 3rd program work.
#print "The Calculated answer is: ", postfix_calculator(tokens)

#------------------------------------------------------------------------------

#This is the 4th Task to store incoming doubles.
#Since this task requires special explaination...
#My code is randomly generating single integer values (assume it to be from the sensor).
#Then it's adding the double of those singles to the stack.
#As soon as the length of the stack reaches 64...
#It presents an option to the user(you) to...
#add a new double from the sensor or keep the stack full.

def capture():

    stack = []
    while True:
        try:
            #I'll just assume the range of the recieving single between 1 - 100.
            single = random.randint(0,100)
            #We need to append a double of the single.
            #We wait for 0.1 second.
            time.sleep(0.1)
            stack.append(single*2)
            print "The current stack of doubles is: ", stack
            if len(stack) == 64:
                user_wish = raw_input("Do you wish to add another double ? ")
                if len(stack) > 64: stack.remove[0]
                if user_wish.lower() == 'yes' or user_wish.lower() == 'y': continue
            else:
                print "The current stack of doubles is: ", stack
                break
        except KeyboardInterrupt, e:
            print "\nUser warranted an interrupt."
            input("Press any key to end...")
            break

#Remove the hash(#) from next line in order to make the 4th program work.
#capture()

#Thank You for your due consideration.
#Hope the script was readable and understandable enough.
#Contact me in case of any discrepancy.
Mrinal Wahal
Mrinal Wahal
Mrinal Wahal
FalconShock
